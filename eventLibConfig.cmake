
if (TARGET EventLib::EventLib)
    return()
endif()

add_library(EventLib::EventLib INTERFACE IMPORTED)

target_include_directories(EventLib::EventLib INTERFACE ${CMAKE_CURRENT_LIST_DIR})

if (NOT DEFINED EventLib_FIND_QUIETLY)
    message("Found EventLib: ${CMAKE_CURRENT_LIST_DIR}")
endif()