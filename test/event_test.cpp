#include <gtest/gtest.h>
#include "../event_handler.h"

#define EVENT_LIB EventLibTests

using namespace EventLib;

class A
{
public:
    A();
    void Call(int a) {
        _handler.Fire(Event<int>(this, a));
    }

    RestrictedEventHandler<int> OnCall;
private:
    EventHandler<int> _handler;
};

A::A(): _handler(this), OnCall(_handler)
{
}


TEST(EVENT_LIB, ShouldPassWithBasicEvent) {
    int correctValue = 99;    
    int testValue = 0;

    EventHandler<int> handler;

    handler.Listen([&](Event<int> event)  {
        testValue = event.value();
    });

    Event<int> e(nullptr, correctValue);
    handler.Fire(e);

    ASSERT_EQ(testValue, correctValue);
}

TEST(EVENT_LIB, ShouldPassWithRestrictedEvent) {
    int correctValue = 99;    
    int testValue = 0;

    EventHandler<int> handler;
    RestrictedEventHandler<int> rHandler(handler);

    rHandler.Listen([&](Event<int> event)  {
        testValue = event.value();
    });

    Event<int> e(nullptr, correctValue);
    handler.Fire(e);

    ASSERT_EQ(testValue, correctValue);
}

TEST(EVENT_LIB, ShouldPassWithEncapsulatedHandlers) {
    A a;
    int correctValue = 99;    
    int testValue = 0;
    bool locked = true;

    a.OnCall += ([&](Event<int> event)  {
        testValue = event.value();
        locked = false;
    });

    a.Call(correctValue);

    ASSERT_EQ(testValue, correctValue);
}